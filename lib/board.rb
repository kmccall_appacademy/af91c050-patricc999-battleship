class Board

  UNDAMAGED_SHIP = :s
  DESTROYED_SPACE = :x

  attr_accessor :grid

  def self.default_grid
    @grid = Array.new(10) { Array.new(10) }
  end

  def initialize(grid = Board.default_grid)
    @grid = grid
  end

  def [](pos)
    x, y = pos
    @grid[x][y]
  end

  def []=(pos, val)
    x,y = pos
    @grid[x][y] = val
  end

  def count
    ship_count = 0
    grid.flatten.each do |space|
      if !space.nil?
        ship_count += 1
      end
    end
    ship_count
  end

  def empty?(position = nil)
    if position.nil? && count == 0
      true
    elsif position.nil? && count != 0
      false
    elsif grid[position[0]][position[1]] == UNDAMAGED_SHIP
      false
    else
      true
    end
  end

  def full?
    if count == grid.flatten.length
      true
    else
      false
    end
  end

  def place_random_ship
    if full?
      raise 'Board is full'
    end

    position = [rand(grid.length), rand(grid[0].length)]

    until empty?(position)
      position = [rand(grid.length), rand(grid[0].length)]
    end

    self[position] = UNDAMAGED_SHIP

  end

  def won?
    empty?
  end

end
